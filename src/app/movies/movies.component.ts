import { Component, OnInit } from '@angular/core';
import { MovieComponent } from '../movie/movie.component';
import { AngularFireList, AngularFireDatabase} from '@angular/fire/database';
import {MatTableModule} from '@angular/material/table';



@Component({
  selector: 'movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  displayedColumns : string[] =['id', 'title', 'Studio', 'Weekend_income','delete'];

  name = "no movie";
  studio ="";
  movies =[];
  studios = [];



  constructor(private db:AngularFireDatabase) { }

  ngOnInit() {
    this.db.list('/movies/').snapshotChanges().subscribe(
      movies=>{
        this.movies = [];
        this.studios =['all'];
        movies.forEach(
          movie =>{
            let y = movie.payload.toJSON();
           // let z = y['Studio'];
          //  y["$key"] = movie.key;
            this.movies.push(y);
            let stu = y['Studio'];
            if (this.studios.indexOf(stu) == -1) {
              this.studios.push(y['Studio']);
            }
          }
        )
      }
    )
  }

  toDelete(element)
  {
    let x,y = this.movies;
    let z = element.id;

    if (this.movies.length == 1)
    {
      this.movies = [];
    }
    if (z > this.movies.length-1)
    {
      z = this.movies.length-1;
    }
    
    x = this.movies.slice(0,z-1);
    y = this.movies.slice(z, this.movies.length);
    this.movies = x;
    this.movies = this.movies.concat(y);
    this.name = element.title;
   
  }

  hideHim($event) {
    this.name = $event.title;
    console.log(this.name);
   // document.getElementById($event.id).style.display = 'none';
 }

 changeStudio(){
  this.db.list('/movies/').snapshotChanges().subscribe(
    movies=>{
      this.movies = [];
      movies.forEach(
        movie =>{
          let y = movie.payload.toJSON();
        if (y['Studio']== this.studio){
          this.movies.push(y);}
        }
      )
    }
  )
 }


}
